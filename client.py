import socket
import time

def main():
    server_name = "127.0.0.1"
    server_port = 2020
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((server_name, server_port))
    print("Untuk keluar dari program ini, masukan angka -1")
    while True:
        sentence = input("Masukan perintah (masukan HELP untuk melihat fitur yang tersedia): ")
        print("=============")
        client_socket.send(sentence.encode())
        reply = client_socket.recv(1024).decode()
        if reply == 'END':
            print("Program Selesai")
            break
        elif reply == "Mulai Antrian":
            counter = 0
            lst_job = []
            while counter < 10:
                sentence = input("Masukan perintah (masukan SELESAI untuk menutup antrian): ")
                lst_job.append(sentence)
                if sentence == "SELESAI":
                    break
                counter += 1
            if counter >= 10:
                lst_job.append("SELESAI")
            send = "-".join(lst_job)
            client_socket.send(send.encode())
            print("=============")
            reply = client_socket.recv(1024).decode()
            while True:
                print_reply(reply)
                if "seluruh pekerjaan" in reply:
                    break
                reply = client_socket.recv(1024).decode()
        else:    
            print_reply(reply)
    client_socket.close()
    
def print_reply(rep):
    print('Dari server:')
    print(rep)
    print("==============")

if __name__ == '__main__':
    main()
