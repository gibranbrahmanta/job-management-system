import socket
import math
import time
import threading

QUEUE_LIMIT = 20
commands_queue = []


class Command(object):
    def __init__(self, command, conn):
        self.command = command
        self.conn = conn

class CommandsThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        threading.Thread.__init__(self)
        self._is_running = False
        self.target = target
        self.name = name

    def run(self):
        self._is_running = True
        while self._is_running:
            
            if len(commands_queue) > 0:
                command = commands_queue.pop(0)
                if type(command) == list:
                    if len(command) > 0:
                        client = command[0].conn
                        total_execution_time = 0
                        try:
                            for i in range(len(command)):
                                start = time.time()
                                res = str(process(command[i].command))
                                end = time.time()
                                tmp_time = end - start
                                total_execution_time += tmp_time
                                job_num = "Pekerjaan ke {}".format(i+1)
                                time_execution_prompt = "Total waktu eksekusi: {} detik".format(
                                    tmp_time)
                                reply = "{}\n{}\n{}\n".format(
                                    job_num, res, time_execution_prompt)
                                client.send(reply.encode())
                            total_time_execution_prompt = "Total waktu eksekusi seluruh pekerjaan: {} detik".format(
                                total_execution_time)
                            client.send(total_time_execution_prompt.encode())
                        except:
                            client.close()
                else:
                    client = command.conn
                    command_data = command.command
                    reply = ""
                    try:
                        if command_data == "-1":
                            client.send("END".encode())
                            client.close()
                        else:
                            start = time.time()
                            res = str(process(command_data))
                            end = time.time()
                            time_execution = end - start
                            time_execution_prompt = "Total waktu eksekusi: {} detik".format(
                                time_execution)
                            reply = "{}\n{}\n".format(res, time_execution_prompt)
                            client.send(reply.encode())
                    except:
                        client.close()

    def stop(self):
        self._is_running = False


class ClientThread(threading.Thread):
    def __init__(self, conn, ip, port):
        threading.Thread.__init__(self)
        self._is_running = False
        self.conn = conn
        self.ip = ip
        self.port = port
        print("[+] Server socket baru untuk " + ip + ":" + str(port))

    def run(self):
        self._is_running = True
        while self._is_running:
            try:
                command = self.conn.recv(1024).decode()
                if command == "ANTRIAN":
                    self.conn.send("Mulai Antrian".encode())
                    lst_job = []
                    command_list = self.conn.recv(1024).decode()
                    command_list = command_list.split("-")
                    for command in command_list:
                        if command == "SELESAI":
                            break
                        else:
                            lst_job.append(Command(command, self.conn))
                    if len(commands_queue) <= QUEUE_LIMIT and len(lst_job) > 0:
                        commands_queue.append(lst_job)
                    else:
                        self.conn.send("Queue sedang penuh".encode())
                elif len(commands_queue) <= QUEUE_LIMIT:
                    commands_queue.append(Command(command, self.conn))
                else:
                    self.conn.send("Queue sedang penuh".encode())
            except:
                self.stop()

    def stop(self):
        self._is_running = False


def main():
    server_port = 2020
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('', server_port))
    threads = []

    for i in range(5):
        c = CommandsThread(name='Commands {}'.format(i+1))
        c.start()
        threads.append(c)

    print("Server sudah siap untuk menerima pekerjaan!")
    while True:
        server_socket.listen(2)
        client, addr = server_socket.accept()
        new_client_thread = ClientThread(client, addr[0], addr[1])
        new_client_thread.start()
        threads.append(new_client_thread)
        
def process(command):
    try:
        command = command.split()
        if len(command) == 0:
            return ""
        elif command[0] == "FIBO_2N":
            if len(command) < 2:
                return 'Masukan Perintah Salah\n \
                FIBO_2N [VALUE]'
            if int(command[1]) > 40:
                return "Angka FIBO_2N harus <= 40"
            res = fibo_2n(int(command[1]))
            return res

        elif command[0] == "BINSER":
            if len(command) < 4:
                return 'Masukan Perintah Salah\n \
                BINSER [VALUE] [START] [END]'
            value = int(command[1])
            start = int(command[2])
            end = int(command[3])

            if end - start > 10000000:
                return "List terlalu besar"

            res = binser(value, start, end)
            return res

        elif command[0] == "LEN_EXPO":
            if len(command) < 3:
                return 'Masukan Perintah Salah\n \
                LEN_EXPO [NUMBER] [AMOUNT]'
            res = expo(int(command[1]), int(command[2]))
            res = math.log(res, 10)
            res = convert_log_result(res) + 1
            return res

        elif command[0] == "LEN_FAST_EXPO":
            if len(command) < 3:
                return 'Masukan Perintah Salah\n \
                    LEN_FAST_EXPO [NUMBER] [AMOUNT]'
            res = fast_expo(int(command[1]), int(command[2]))
            res = math.log(res, 10)
            res = convert_log_result(res) + 1
            return res

        elif command[0] == "HELP":
            res = ""
            res += "Untuk mencari angka Fibonaci ke n (n bernilai maksimal 40), gunakan perintah FIBO_2N n \n"
            res += "Untuk melakukan Binary Search angka n dari range angka a hingga b, gunakan perintah BINSER n a b \n"
            res += "Untuk banyaknya digit dari hasil m^n dengan metode normal, gunakan perintah LEN_EXPO m n \n"
            res += "Untuk banyaknya digit dari hasil m^n dengan metode cepat, gunakan perintah LEN_FAST_EXPO m n \n"
            res += "Untuk mengirim beberapa pekerjaan sekaligus, gunakan perintah ANTRIAN \n"
            res += "Untuk keluar dari program ini, masukan -1"
            return res

        return "Perintah tidak ditemukan"
    except:
        return "Perintah tidak valid"


def fibo_2n(number):
    if number == 0:
        return 0
    elif number == 1:
        return 1
    else:
        return fibo_2n(number-1) + fibo_2n(number-2)


def binser(value, start, end):
    lst = [i for i in range(start, end+1)]

    low = 0
    high = len(lst) - 1
    mid = 0

    while low <= high:
        mid = (high + low) // 2
        if lst[mid] < value:
            low = mid + 1
        elif lst[mid] > value:
            high = mid - 1
        else:
            return mid
    return -1


def expo(p, q):
    r = 1
    for i in range(q):
        r = r*p
    return r


def fast_expo(a, b):
    return a ** b


def convert_log_result(number):
    if number % 1 > 0.9999:
        return math.ceil(number)
    return math.floor(number)


if __name__ == '__main__':
    main()
